﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hava_durumu.Models;
using Newtonsoft.Json;

namespace Hava_durumu.Controllers
{
    [Authorize]
    public class CommentController : Controller
    {
        // GET All Comments: Comment
        public JsonResult Get(string language, string city)
        {
            var db = new ApplicationDbContext();
            //var weatherInfos = db.WeatherInfos.Where(x => x.City.Name == city);
            IEnumerable<Comment> Comments = db.Comments
                .Join(db.WeatherInfos, comment => comment.WeatherInfoID
                , weatherInfo => weatherInfo.ID
                , (comment, weatherInfo) => comment)
                .OrderByDescending(x => x.Date)
                .Where(x => x.Language == language).Take(10).ToList();
            var result = new List<ReturnedComment>();
            foreach(var item in Comments)
            {
                var res = new ReturnedComment()
                {
                    CommentID = item.ID,
                    Content = item.Content,
                    Date = item.Date,
                    Language = item.Language,
                    Writer = db.PersonalUsers.First(x => x.ID == item.ID).Name
                };
                result.Add(res);
            }
            return Json(result);
        }

        //public JsonResult Get(int id)
        //{
        //    return null;
        //}

        public JsonResult Save(CommentAjax ajax)
        {
            if (!ModelState.IsValid)
            {
                return null;
            }
            var comment = JsonConvert.DeserializeObject<CommentSaveModel>(ajax.Data);
            var db = new ApplicationDbContext();
            var userID = db.PersonalUsers.Single(user => user.Email == comment.WriterName).ID;
            var commentToSave = new Comment()
            {
                Content = comment.Content,
                Language = comment.Language,
                Date = DateTime.Now,
                WeatherInfoID = comment.WeatherInfoID,
                WriterID = userID
            };

            db.Comments.Add(commentToSave);
            db.SaveChanges();
            db.Dispose();

            return Json(commentToSave);
        }

        public class ReturnedComment
        {
            public int CommentID { get; set; }
            public string Content { get; set; }
            public string Language { get; set; }
            public DateTime Date { get; set; }
            public string Writer { get; set; }
        }
    }
}