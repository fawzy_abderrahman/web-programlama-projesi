﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Threading;
using System.Globalization;
using Hava_durumu.Models;
using System.Net;

namespace Hava_durumu.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string language)
        {
            SetCulture(language);

            //var db = new CommentDBContext();
            //var user = new User();
            //user.FirstName = "Fawzy";
            //user.LastName = "Abderrahman";
            //user.Name = "Fawzy Abderrahman";
            //user.Email = "fawzy@example.com";
            //db.Users.Add(user);

            //var comment = new Comment();
            //comment.Date = DateTime.Now;
            //comment.Title = "Test1";
            //comment.WritingUser = user;
            //db.Comments.Add(comment);
            //db.SaveChanges();
            //db.Dispose();

            //var appid = "98b67b8f5333c2991374b3e33cbee535";
            //var lat = 63.22;
            //var lon = 30.27;
            //var url = string.Format("http://api.openweathermap.org/data/2.5/weather?units=metric&lat={0}&lon={1}&appid={2}", lat, lon, appid);

            //using (WebClient client = new WebClient())
            //{
            //    string json = client.DownloadString(url);
            //    JObject ob = new JObject(json);
            //    return Content(json);
            //}

            ViewBag.Lang = language;

            return View(new Search());
        }

        [HttpPost]
        public ActionResult Index(/*Search data*/ string SearchAjax, string language)
        {
            SetCulture(language);

            var data = JsonConvert.DeserializeObject<Search>(SearchAjax);
            Coordinates coord = null;
            //var data = AppModel.Search;
            if (data.City != null && data.City.ToLower() != Hava_durumu.Languages.lang.WeatherInfosCity.ToLower())
            {
                coord = GetCityCoord(data.City);
            }
            if (coord == null)
            {
                coord = new Coordinates();
                if (data.Lat != null && data.Lat != 0.0 && data.Lon != null && data.Lon != 0.0)
                {
                    coord.Lat = (double)data.Lat;
                    coord.Lon = (double)data.Lon;
                }
                else
                {
                    coord.Lat = (double)data.OldLat;
                    coord.Lon = (double)data.OldLon;
                }
            }
            
            ViewBag.Lang = language;
            ViewBag.Lat = coord.Lat;
            ViewBag.Lon = coord.Lon;
            return View(new Search());
        }

        public ActionResult About(string language)
        {
            SetCulture(language);

            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact(string language)
        {
            SetCulture(language);

            ViewBag.Message = "Your contact page.";

            return View();
        }

        public void SetCulture(string language)
        {
            if (Thread.CurrentThread.CurrentCulture.Name != language)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(language);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
            }
        }
        public Coordinates GetCityCoord(string cityName)
        {
            //var db = new ApplicationDbContext();
            var city = new City();
            //var city = db.Cities.First(x => x.Name.ToLower().Contains(cityName.ToLower()));
            //if (city != null)
            //{
            //    db.Dispose();
            //    return city.Coord;
            //}
            //else
            //{
            string path = Server.MapPath("~/Data/city-list.json");
            var file = new System.IO.StreamReader(path);
            string line;
            while (!file.EndOfStream)
            {
                line = file.ReadLine();
                //var job = (JObject)JsonConvert.DeserializeObject(line);
                //Console.WriteLine(line);
                //Console.WriteLine("id:{0}", job.Value<int>("id"));
                //Console.WriteLine("name:{0}", job.Value<string>("name"));
                //Console.WriteLine("country:{0}", job.Value<string>("country"));
                //Console.WriteLine("coord: lat:{0} lon:{1}", job.Value<JToken>("coord").Value<double>("lat"), job.Value<JToken>("coord").Value<double>("lon"));
                city = JsonConvert.DeserializeObject<City>(line);
                //Console.WriteLine(line);
                //Console.WriteLine("id:{0}", city.ID);
                //Console.WriteLine("name:{0}", city.Name);
                //Console.WriteLine("country:{0}", city.Country);
                //Console.WriteLine("coord: lat:{0} lon:{1}", city.Coord.Lat, city.Coord.Lon);
                if (TrimSpaceAndLower(city.Name).StartsWith(TrimSpaceAndLower(cityName)))
                {
                    //            db.Cities.Add(city);
                    //            db.SaveChanges();
                    //            db.Dispose();
                    return city.Coord;
                }
                //    }  
            }
            return null;
        }

        public string TrimSpace(string text)
        {
            string[] res = text.Split(' ');
            string result = "";
            foreach(var item in res)
            {
                result += item;
            }
            return result;
        }
        public string TrimSpaceAndLower(string text)
        {
            string[] res = text.Split(' ');
            string result = "";
            foreach (var item in res)
            {
                result += item;
            }
            return result.ToLower();
        }
    }
}