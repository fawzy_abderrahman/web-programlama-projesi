﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Hava_durumu.Models;
using Newtonsoft.Json;

namespace Hava_durumu.Controllers
{
    public class WeatherController : Controller
    {
        // GET: Weather
        [HttpPost]
        public JsonResult Save(WeatherAjax weatherAjax)
        {
            if (ModelState.IsValid)
            {
                var db = new ApplicationDbContext();
                WeatherInfo weatherInfo;
               
                var weather = JsonConvert.DeserializeObject<WeatherSaveModel>(weatherAjax.Data);

                try
                {
                    weatherInfo = db.WeatherInfos.DefaultIfEmpty(null).OrderBy(x => x.Date).Where(x => x.City.Name == weather.Name && x.Language == weather.Language).LastOrDefault(x => (Math.Abs(x.Date.Ticks - DateTime.Now.Ticks)) < 6000000000);
                }
                catch (Exception)
                {
                    weatherInfo = null;
                }
                if (weatherInfo == null)
                {
                    //var city = new City()
                    //{
                    //    ID = weather.CityID,
                    //    Country = weather.Country,
                    //    Name = weather.Name,
                    //    Coord = new Coordinates()
                    //    {
                    //        Lat = weather.Lat,
                    //        Lon = weather.Lon
                    //    }
                    //};
                    var city = GetCity(weather.Name);
                    //var infoWeather = new Weather()
                    //{
                    //    Id = weather.WeatherInfoId,
                    //    Description = weather.Description,
                    //    Icon = weather.Icon,
                    //    Main = weather.Main
                    //};
                    var infoWeather = GetWeather(weather.Main, weather.Icon, weather.Language, weather.Description);

                    var values = new WeatherValues()
                    {
                        Clouds = weather.Clouds,
                        Humidity = weather.Humidity,
                        Temp = weather.Temp,
                        TempMax = weather.TempMax,
                        TempMin = weather.TempMin,
                        Winds = weather.Winds
                    };
                    weatherInfo = new WeatherInfo()
                    {
                        CityID = city.ID,
                        Date = DateTime.Now,
                        WeatherID = infoWeather.Id,
                        Values = values,
                        Code = weather.Code,
                        Language = weather.Language
                    };

                    db.WeatherInfos.Add(weatherInfo);
                    db.SaveChanges();
                    db.Dispose();
                }
                return Json(new { Lat = weather.Lat
                , Lon = weather.Lon
                , City = weather.Name
                , Country = weather.Country });
            }
            return null;
        }

        public City GetCity(string cityName)
        {
            var db = new ApplicationDbContext();
            City city = null;
            try
            {
                city = db.Cities.DefaultIfEmpty(null).FirstOrDefault(x => x.Name.ToLower().StartsWith(cityName.ToLower()));
            }
            catch (Exception)
            {
                city = null;
            }
            if (city != null)
            {
                db.Dispose();
                return city;
            }
            else
            {
                string path = Server.MapPath("~/Data/city-list.json");
                var file = new System.IO.StreamReader(path);
                string line;
                while (!file.EndOfStream)
                {
                    line = file.ReadLine();
                    city = JsonConvert.DeserializeObject<City>(line);
                    if (city.Name.ToLower().StartsWith(cityName.ToLower()))
                    {
                        db.Cities.Add(city);
                        db.SaveChanges();
                        db.Dispose();
                        return city;
                    }
                }
            }
            return null;
        }

        public Weather GetWeather(string main, string icon, string language, string description)
        {
            var db = new ApplicationDbContext();
            Weather weather;
            try
            {
                weather = db.Weathers.DefaultIfEmpty(null).First(x => x.Main == main && x.Icon == icon && x.Language == language);
            }
            catch (Exception)
            {
                weather = null;
            }
            if (weather == null)
            {
                weather = new Weather() { Icon = icon, Main = main, Language = language, Description = description };
                db.Weathers.Add(weather);
                db.SaveChanges();
                db.Dispose();
            }
            return weather;
        }

        public string TrimSpace(string text)
        {
            string[] res = text.Split(' ');
            string result = "";
            foreach (var item in res)
            {
                result += item;
            }
            return result;
        }
        public string TrimSpaceAndLower(string text)
        {
            string[] res = text.Split(' ');
            string result = "";
            foreach (var item in res)
            {
                result += item;
            }
            return result.ToLower();
        }

        //For test purpose
        public class test
        {
            public string Name { get; set; }
            public string LastName { get; set; }
        }
    }
}