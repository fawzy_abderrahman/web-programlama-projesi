﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Hava_durumu.Models
{
    public class Comment
    {
        [Required]
        public int ID { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public string Language { get; set; }
        public int WriterID { get; set; }
        public int WeatherInfoID { get; set; }

        [ForeignKey("WriterID")]
        public virtual User Writer { get; set; }
        [ForeignKey("WeatherInfoID")]
        public virtual WeatherInfo WeatherInfo { get; set; }
        
    }

    public class User
    {
        [Required]
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }

        public virtual List<Comment> Comments { get; }
    }

    //public class CommentDBContext : DbContext
    //{
    //    public CommentDBContext() : base("CommentDBContext")
    //    {

    //    }
    //    public DbSet<Comment> Comments { get; set; }
    //    public DbSet<User> Users { get; set; }
    //    public DbSet<WeatherInfo> WeatherInfos { get; set; }
    //}
}