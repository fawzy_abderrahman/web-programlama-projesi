﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hava_durumu.Models
{
    public class AppIndexViewModel
    {
        //public WeatherSaveModel SaveWeather { get; set; }
        //public string JsonString { get; set; }
        public Search Search { get; set; }
        public CommentSaveModel SaveComment { get; set; }
        public IEnumerable<Comment> Comments { get; set; }

    }
    public class WeatherSaveModel
    {
        public double Lat { get; set; }
        public double Lon { get; set; }
        public string Icon { get; set; }
        public double Temp { get; set; }
        public double TempMin { get; set; }
        public double TempMax { get; set; }
        public double Humidity { get; set; }
        public string Main { get; set; }
        public string Description { get; set; }
        public double Winds { get; set; }
        public double Clouds { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public int CityID { get; set; }
        public int Code { get; set; }
        public int WeatherInfoId { get; set; }
        public string Language { get; set; }
    }

    public class WeatherAjax
    {
        public string Data { get; set; }
    }

    public class CommentAjax
    {
        public string Data { get; set; }
    }
    
    public class CommentSaveModel
    {
        public string WriterName { get; set; }
        public string Content { get; set; }
        public string City { get; set; }
        public string Language { get; set; }
        public int WeatherInfoID { get; set; }
    }

    public class Search
    {
        public string City { get; set; }
        public double? Lat { get; set; }
        public double? Lon { get; set; }
        public double OldLat { get; set; }
        public double OldLon { get; set; }
    }

    public class SearchAjax
    {
        public string Data { get; set; }
    }
}