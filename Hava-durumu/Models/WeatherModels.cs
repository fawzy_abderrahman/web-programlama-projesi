﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Hava_durumu.Models
{
    public class Coordinates
    {
        public double Lon { get; set; }
        public double Lat { get; set; }
    }

    public class Weather
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public string Main { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public string Language { get; set; }

        public virtual List<WeatherInfo> WeatherInfos { get; set; }
    }

    public class Main
    {
        public double Temp { get; set; }
        public double Pressure { get; set; }
        public double Humidity { get; set; }
        public double TempMin { get; set; }
        public double TempMax { get; set; }
        public double Wind { get; set; }
        public double Cloud { get; set; }
        public double Rain { get; set; }
    }

    public class Sys
    {
        public int Type { get; set; }
        public int ID { get; set; }
        public string Message { get; set; }
        public DateTime Sunrise { get; set; }
        public DateTime Sunset { get; set; }
    }

    public class WeatherInfo
    {
        public int ID { get; set; }
        public WeatherValues Values { get; set; }
        public DateTime Date { get; set; }
        public int Code { get; set; }
        public string Language { get; set; }
        public int CityID { get; set; }
        public int WeatherID { get; set; }

        [ForeignKey("CityID")]
        public virtual City City { get; set; }
        [ForeignKey("WeatherID")]
        public virtual Weather Weather { get; set; }
        public virtual List<Comment> Comments { get; set; }
        //public List<List> List { get; set; }
    }

    public class City
    {
        //[DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ID { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public Coordinates Coord { get; set; }

        public virtual List<WeatherInfo> Weather { get; set; }
    }

    public class WeatherValues
    {
        public double Temp { get; set; }
        public double Humidity { get; set; }
        public double TempMin { get; set; }
        public double TempMax { get; set; }
        public double Winds { get; set; }
        public double Clouds { get; set; }
    }

    //public class Temperature
    //{
    //    public double Day { get; set; }
    //    public double Min { get; set; }
    //    public double Max { get; set; }
    //    public double Night { get; set; }
    //}

    

    //public class List
    //{
    //    public Temperature Temp { get; set; }
    //    public int Humidity { get; set; }
    //    public List<Weather> Weather { get; set; }
    //}
}