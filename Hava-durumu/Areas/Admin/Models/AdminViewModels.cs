﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Hava_durumu.Areas.Admin.Models
{
    public class UsersViewModel
    {
        public string UserName { get; set; }

        [Display(ResourceType = typeof(Languages.lang), Name = "AdminUsersFormUserName")]
        public bool UserNameCheckbox { get; set; }

        [Display(ResourceType = typeof(Languages.lang), Name = "FormRemember")]
        public bool RoleCheckbox { get; set; }
    }

    public class DeleteUserViewModel
    {
        [Required]
        public string UserName { get; set; }
    }

    public class ModifyUserViewModel
    {
        [Required]
        public string UserName { get; set; }
        public string Role { get; set; }
    }

    public class CommentsViewModel
    {
        [Display(ResourceType = typeof(Languages.lang), Name = "AdminUsersFormUserName")]
        public bool UserNameCheckbox { get; set; }

        [Display(ResourceType = typeof(Languages.lang), Name = "AdminUsersFormDate")]
        public bool DateCheckbox { get; set; }

        [Display(ResourceType = typeof(Languages.lang), Name = "AdminUsersFormCity")]
        public bool CityCheckbox { get; set; }
    }

    public class DeleteComment
    {
        [Required]
        public int ID { get; set; }
    }

    public class User
    {
        public string UserName { get; set; }
        public string Role { get; set; }
    }

    public class Comment
    {
        public int ID { get; set; }
        public string Content { get; set; }
        public string WriterName { get; set; }
        public DateTime Date { get; set; }
        public string City { get; set; }
    }

    public class IndexViewModel
    {
        public IEnumerable<User> Users { get; set; }
        public IEnumerable<Comment> Comments { get; set; }
    }
}