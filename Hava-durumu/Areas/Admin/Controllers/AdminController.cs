﻿using Hava_durumu.Areas.Admin.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Hava_durumu.Areas.Admin.Controllers
{
    [Authorize(Roles = "Admin", Users = "Fawzy Abderrahman")]
    public class AdminController : Controller
    {
        // GET: Admin/Admin
        public ActionResult Index(string language)
        {
            SetCulture(language);

            //db = new Hava_durumu.Models.ApplicationDbContext();
            IndexViewModel model = new IndexViewModel();
            model.Users = new List<User>() { new Models.User { UserName = "fawzy", Role = "normal" },
                new Models.User { UserName = "rıdvan", Role = "normal" } };
            model.Comments = new List<Comment>() { new Comment { ID = 1, City = "Sakarya", Content = "Hello world", Date = DateTime.Now, WriterName = "fawzy" },
                new Comment { ID = 2, City = "Paris", Content = "Quel beau temps il fait!", Date = DateTime.Now, WriterName = "fawzy" }
            };
            return View(model);
        }

        public void SetCulture(string language)
        {
            if (Thread.CurrentThread.CurrentCulture.Name != language)
            {
                Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(language);
                Thread.CurrentThread.CurrentUICulture = new CultureInfo(language);
            }
        }
    }
}