﻿var map;
var center;
var mapOptions;
var marker;
var searchText = "";
var weatherPosition;
var weather;

$(document).ready(function () {
    searchText = $('input[id="search-bar"]').val();

    $('#tab a').click(function (e) {
        $(this).tab('show');
    });
    //var timerID = setTimeout(function () {
    //    var text = center ? 'lat: ' + center.lat() + '<br>lon: ' + center.lng() : 'Error: center not defined!';
    //    $('#weather-infos p').html(text);
    //}, 1000);
    $('input[id="search-bar"]').focusin(function () {
        //if (!searchText) {
        //    searchText = $(this).val();
        //}
        $(this).val('');
    });
    $('input[id="search-bar"]').focusout(function () {
        if (!$(this).val()) {
            $(this).val(searchText);
        }
    });
    var j;
    $('#search-btn').hover(function () {
        j = {
            "City": $('input[id="search-bar"]').val(),
            "Lat": Number(replaceDots($('#new-city-lat').val())),
            "Lon": Number(replaceDots($('#new-city-lon').val())),
            "OldLat": Number(replaceDots($('#old-city-lat').val())),
            "OldLon": Number(replaceDots($('#old-city-lon').val()))
        };
        if ($('#SearchAjax').val()) {
            $('#SearchAjax').val('');
        }
        $('#SearchAjax').val(JSON.stringify(j));
        //if ($('#search-bar').val() == searchText) {
        //    $('#search-bar').val(JSON.stringify(j));
        //}
    });
    var url = "http://localhost:5387/" + lang + "/Home/Index";
    $('#search-btn').click(function () {
        $('#execute-search').attr('action', url);
        $('#execute-search').submit();
    });

    $('.search-trigger').hover(function () {
        j = {
            "City": $('.search-trigger-box').val(),
            "Lat": Number(replaceDots($('#new-city-lat').val())),
            "Lon": Number(replaceDots($('#new-city-lon').val())),
            "OldLat": Number(replaceDots($('#old-city-lat').val())),
            "OldLon": Number(replaceDots($('#old-city-lon').val()))
        };
        if ($('#SearchAjax').val()) {
            $('#SearchAjax').val('');
        }
        $('#SearchAjax').val(JSON.stringify(j));
        //if ($('#search-bar').val() == searchText) {
        //    $('#search-bar').val(JSON.stringify(j));
        //}
    });
    $('.search-trigger').click(function () {
        $('#execute-search').attr('action', url);
        $('#execute-search').submit();
    });


    //$('#test').click(function () {
    //    $.ajax({
    //        type: 'POST',
    //        url: url,
    //        data: { "Data": JSON.stringify(j) },
    //        dataType: 'application/json',
    //        async: false
    //    });
    //    //$.post("http://localhost:5387/" + lang + "/Home/Index", { Data: $('#json-string').val(JSON.stringify(j)) });
    //});
    
});

function initMap() {
    //Declaring the vars.
    var defaultIstanbul = new google.maps.LatLng(41.06, 28.99);
    var mapCanvas;

    if (lat == null || lat == "" || lon == null || lon == "") {
        //Is geolocation available
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                //Centering the map to the current location
                center = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                mapCanvas = document.getElementById('map-canvas');
                mapOptions = {
                    center: center,
                    zoom: 7
                }
                weatherPosition = { lat: position.coords.latitude, lon: position.coords.longitude };
                map = new google.maps.Map(mapCanvas, mapOptions);
                marker = new google.maps.Marker({
                    position: center,
                    map: map
                });

                //Weather functions calls
                getCurrentWeatherInfoByCoord(weatherPosition, displayCurrentWeatherInfo);

                google.maps.event.addListener(map, 'click', function (e) {
                    if (marker.map) {
                        marker.setMap(null);
                        removeSearchInfos();
                    }
                    else {
                        placeMarkerAndPanTo(e.latLng, map);
                    }
                });
                google.maps.event.addListener(marker, 'click', function () {
                    var infowindow = new google.maps.InfoWindow({
                        content: "Hava-durumu"
                    });
                    infowindow.open(map, marker);
                });
            });
        }
        else {
            //Defaulting the center of the map to Istanbul
            center = defaultIstanbul;
            mapCanvas = document.getElementById('map-canvas');
            mapOptions = {
                center: center,
                zoom: 7
            }
            //Weather functions calls
            weatherPosition = { lat: center.lat(), lon: center.lng() };
            getCurrentWeatherInfoByCoord(weatherPosition, displayCurrentWeatherInfo);

            map = new google.maps.Map(mapCanvas, mapOptions);
            marker = new google.maps.Marker({
                position: center,
                map: map
            });
            google.maps.event.addListener(map, 'click', function (e) {
                if (marker.map) {
                    marker.setMap(null);
                    removeSearchInfos();
                }
                else {
                    placeMarkerAndPanTo(e.latLng, map);
                }
            });
        }
    }
    else {
        console.log("lat:" + lat/*.toString() */ + " lon:" + lon/*.toString()*/);
        var newLat = lat.replace(',', '.');
        var newLon = lon.replace(',', '.');
        var a = parseFloat(newLat);
        var b = parseFloat(newLon);
        var c = a + b;
        console.log(a);
        console.log(b);
        console.log(c);
        center = new google.maps.LatLng(parseFloat(newLat), parseFloat(newLon));
        mapCanvas = document.getElementById('map-canvas');
        mapOptions = {
            center: center,
            zoom: 7
        }
        map = new google.maps.Map(mapCanvas, mapOptions);
        marker = new google.maps.Marker({
            position: center,
            map: map
        });
        //Weather functions calls
        weatherPosition = { lat: center.lat(), lon: center.lng() };
        getCurrentWeatherInfoByCoord(weatherPosition, displayCurrentWeatherInfo);

        google.maps.event.addListener(map, 'click', function (e) {
            if (marker.map) {
                marker.setMap(null);
                removeSearchInfos();
            }
            else {
                placeMarkerAndPanTo(e.latLng, map);
            }
        });
    }
}

function placeMarkerAndPanTo(latLng, map) {
    marker = new google.maps.Marker({
        position: latLng,
        map: map
    });
    setTimeout(function () {
        map.panTo(latLng);
    }, 500);
    setSearchInfos(latLng);
    //$('input[id="search-bar"]').val(searchText);
    //$('input[id="new-city-lat"]').val(latLng.lat());
    //$('input[id="new-city-lon"]').val(latLng.lng());
}

function setSearchInfos(latLng) {
    $('input[id="search-bar"]').val(searchText);
    $('input[id="new-city-lat"]').val(latLng.lat());
    $('input[id="new-city-lon"]').val(latLng.lng());
}

function removeSearchInfos() {
    $('input[id="new-city-lat"]').val(null);
    $('input[id="new-city-lon"]').val(null);
}

function getCurrentWeatherInfoByCoord(coord, display) {
    var url = "http://api.openweathermap.org/data/2.5/weather?units=metric&appid=98b67b8f5333c2991374b3e33cbee535";
    url += "&lat=" + (coord.lat.toPrecision(6)).toString();
    url += "&lon=" + (coord.lon.toPrecision(6)).toString();
    url += "&lang=" + lang.toString();
    console.log("Quering data from weather api");
    console.log(url);
    $.get(url, displayCurrentWeatherInfo);
}

function displayCurrentWeatherInfo(weatherData) {
    console.log("Data ready to use");
    weather = weatherData;
    $('#current-weather-city-country').text(weather.name + ", " + weather.sys.country);
    var imgSrc = "http://openweathermap.org/img/w/" + weather.weather[0].icon + ".png";
    $('#current-weather-image').attr('src', imgSrc);
    var tempText = weather.main.temp + "°C (" + weather.main.temp_min + "°C \<=\> " + weather.main.temp_max + "°C)";
    $('#current-weather-temp').text(tempText);
    $('#current-weather-humidity').text(weather.main.humidity + " %");
    $('#current-weather-main').text(capitalizeFirstLetter(weather.weather[0].main));
    $('#current-weather-description').text(capitalizeFirstLetter(weather.weather[0].description));
    $('#current-weather-wind').text(weather.wind.speed + " m/s");
    $('#current-weather-clouds').text(weather.clouds.all + " %");
    //$('#save-weather-infos-btn').click();
    var saveUrl = "http://localhost:5387/" + lang + "/Weather/Save"/* + $('#save-weather-url').attr('href')*/;
    console.log(saveUrl);
    var data = formatToWeatherSaveModel(weather);
    console.log(data);
    setTimeout(function () {
        $.post(saveUrl, { Data: data }, function (data) {
            //if (data) {
            //    console.log("success");
            //}
            //else {
            //    console.log('faillure');
            //}
            console.log(data);
            $('input[id="old-city-lat"]').val(data.Lat);
            $('input[id="old-city-lon"]').val(data.Lon);
        });
    }, 1500);
    
}

//function getComments(city) {
//    var url = "localhost://5387/" + lang + "/Comment/Get?language=" + lang + "&city=" + city;
//    $.get(url, function (data) {
        
//    });
//}

function formatToWeatherSaveModel(data) {
    console.log(typeof(data.main.temp));
    var j = {
        "Lat": Number(replaceDots(data.coord.lat)),
        "Lon": Number(data.coord.lon.toString()),
        "Icon": data.weather[0].icon,
        "Temp": Number(replaceDots(data.main.temp)),
        "TempMax": Number(data.main.temp_max),
        "TempMin": Number(replaceDots(data.main.temp_min)),
        "humidity": Number(data.main.humidity),
        "Main": capitalizeFirstLetter(data.weather[0].main),
        "Description": capitalizeFirstLetter(data.weather[0].description),
        "Winds": Number(replaceDots(data.wind.speed)),
        "Clouds": Number(data.clouds.all),
        "Name": data.name,
        "Country": data.sys.country,
        "CityID": Number(data.id),
        "Code": Number(data.cod),
        "WeatherInfoId": Number(data.weather[0].id),
        "Language": lang.toString()
    }
    return JSON.stringify(j);
}

function capitalizeFirstLetter(text) {
    return text.charAt(0).toUpperCase() + text.slice(1).toLowerCase();
}

function replaceDots(text) {
    return text.toString().replace(',', '.');
}


////Submit infos
//{ 
//    "coord": { 
//            "lat": weatherData.coord.lat, 
//            "lon": weatherData.coord.lon 
//    },
//    "weather": { "id": weatherData.weather.id, //weather is a list
//            "main": weatherData.weather.main, 
//            "description": weatherData.weather.description,
//            "icon": weatherData.weather.icon 
//    },
//    "main": { "temp": weatherData.main.temp,
//            "pressure": weatherData.main.pressure,
//            "humidity": weatherData.main.humidity,
//            "temp_min": weatherData.main.temp_min,
//            "temp_max": weatherData.main.temp_max 
//    },
//    "wind": { 
//            "speed": weatherData.wind.speed,
//            "deg": weatherData.wind.deg
//    },
//    "clouds":{ "all": weatherData.clouds.all },
//    "city": {
//            "id": weatherData.id,
//            "name": weatherData.name,
//            "country": weatherData.sys.country
//    }

//}


//Is geolocation available
//if (navigator.geolocation) {
//    navigator.geolocation.getCurrentPosition(function (position) {
//        //Centering the map to the current location
//        center = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
//        mapCanvas = document.getElementById('map-canvas');
//        mapOptions = {
//            center: center,
//            zoom: 7
//        }
//        map = new google.maps.Map(mapCanvas, mapOptions);
//        marker = new google.maps.Marker({
//            position: center,
//            map: map
//        });

//        //Weather functions calls
//        getCurrentWeatherInfoByCoord(position);

//        google.maps.event.addListener(map, 'click', function (e) {
//            if (marker.map) {
//                marker.setMap(null);
//            }
//            else {
//                placeMarkerAndPanTo(e.latLng, map);
//            }
//        });
//        google.maps.event.addListener(marker, 'click', function () {
//            var infowindow = new google.maps.InfoWindow({
//                 content: "Hava-durumu"
//            });
//            infowindow.open(map, marker);
//        });
//    });
//} else {
//    //Defaulting the center of the map to Istanbul
//    center = defaultIstanbul;
//    mapCanvas = document.getElementById('map-canvas');
//    mapOptions = {
//        center: center,
//        zoom: 7
//    }
//    map = new google.maps.Map(mapCanvas, mapOptions);
//    marker = new google.maps.Marker({
//        position: center,
//        map: map
//    });
//    google.maps.event.addListener(map, 'click', function (e) {
//        if (marker.map) {
//            marker.setMap(null);
//        }
//        else {
//            placeMarkerAndPanTo(e.latLng, map);
//        }
//    });
//}