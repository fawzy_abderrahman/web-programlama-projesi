﻿using Hava_durumu.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Hava_durumu.Startup))]
namespace Hava_durumu
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesandUsers();
        }

        // In this method we will create default User roles and Admin user for login    
        private void createRolesandUsers()
        {
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            //Creating Normal role     
            if (!roleManager.RoleExists("Normal"))
            {
                var role = new IdentityRole();
                role.Name = "Normal";
                roleManager.Create(role);
                //var user = UserManager.Find("Fawzy Abderrahman", "Fawzy123");
                var enumerator = context.Users.GetEnumerator();
                
                while (enumerator.MoveNext())
                {
                    try
                    {
                        var result1 = UserManager.AddToRole(enumerator.Current.Id, "Normal");
                    }
                    catch (System.Exception)
                    {
                        
                    }
                    
                }
                //if (user != null)
                //{
                //    var result1 = UserManager.AddToRole(user.Id, "Normal");
                //}
            }

            // In Startup iam creating first Admin Role and creating a default Admin User     
            if (!roleManager.RoleExists("Admin"))
            {

                // first we create Admin role    
                var role = new IdentityRole();
                role.Name = "Admin";
                roleManager.Create(role);

                //Here we create a Admin super user who will maintain the website                   
                var user = new ApplicationUser();
                user.UserName = "fawzy9875";
                user.FirstName = "Gamal";
                user.LastName = "Fawzou";
                user.Email = "fawzy.abderrahman9875@gmail.com";

                string userPWD = "Fawzy@9875";

                var chkUser = UserManager.Create(user, userPWD);

                //Add default User to Role Admin    
                if (chkUser.Succeeded)
                {
                    var result1 = UserManager.AddToRole(user.Id, "Admin");
                    var pUser = new User()
                    {
                        Email = user.Email,
                        FirstName = user.FirstName,
                        LastName = user.LastName,
                        Name = user.UserName
                    };
                    context.PersonalUsers.Add(pUser);
                }
            }
        }
    }
}

//var user = new ApplicationUser();
//user.UserName = "fawzy9875";
//            user.Email = "fawzy.abderrahman9875@gmail.com";

//            string userPWD = "Fawzy@9875";

//var chkUser = UserManager.Create(user, userPWD);
//var result1 = UserManager.AddToRole(user.Id, "Admin");