namespace Hava_durumu.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig3 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.WeatherInfoes", "CityID", "dbo.Cities");
            DropForeignKey("dbo.WeatherInfoes", "WeatherID", "dbo.Weathers");
            DropPrimaryKey("dbo.Cities");
            DropPrimaryKey("dbo.Weathers");
            AlterColumn("dbo.Cities", "ID", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Weathers", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.Cities", "ID");
            AddPrimaryKey("dbo.Weathers", "Id");
            AddForeignKey("dbo.WeatherInfoes", "CityID", "dbo.Cities", "ID", cascadeDelete: true);
            AddForeignKey("dbo.WeatherInfoes", "WeatherID", "dbo.Weathers", "Id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.WeatherInfoes", "WeatherID", "dbo.Weathers");
            DropForeignKey("dbo.WeatherInfoes", "CityID", "dbo.Cities");
            DropPrimaryKey("dbo.Weathers");
            DropPrimaryKey("dbo.Cities");
            AlterColumn("dbo.Weathers", "Id", c => c.Int(nullable: false));
            AlterColumn("dbo.Cities", "ID", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.Weathers", "Id");
            AddPrimaryKey("dbo.Cities", "ID");
            AddForeignKey("dbo.WeatherInfoes", "WeatherID", "dbo.Weathers", "Id", cascadeDelete: true);
            AddForeignKey("dbo.WeatherInfoes", "CityID", "dbo.Cities", "ID", cascadeDelete: true);
        }
    }
}
