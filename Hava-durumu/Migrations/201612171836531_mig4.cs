namespace Hava_durumu.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class mig4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WeatherInfoes", "Language", c => c.String());
            AddColumn("dbo.Comments", "Language", c => c.String());
            AddColumn("dbo.Weathers", "Language", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Weathers", "Language");
            DropColumn("dbo.Comments", "Language");
            DropColumn("dbo.WeatherInfoes", "Language");
        }
    }
}
