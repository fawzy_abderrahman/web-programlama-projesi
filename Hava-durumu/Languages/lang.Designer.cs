﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hava_durumu.Languages {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class lang {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal lang() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Hava_durumu.Languages.lang", typeof(lang).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hedefimiz.
        /// </summary>
        public static string AboutMission {
            get {
                return ResourceManager.GetString("AboutMission", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hakkımızda sayfası.
        /// </summary>
        public static string AboutPageTitle {
            get {
                return ResourceManager.GetString("AboutPageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Destek.
        /// </summary>
        public static string AboutSupport {
            get {
                return ResourceManager.GetString("AboutSupport", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kullanım bilgileri.
        /// </summary>
        public static string AboutUsageInformation {
            get {
                return ResourceManager.GetString("AboutUsageInformation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Filtreyi uygula.
        /// </summary>
        public static string AdminPageFormsFilter {
            get {
                return ResourceManager.GetString("AdminPageFormsFilter", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Admin paneli.
        /// </summary>
        public static string AdminPageTitle {
            get {
                return ResourceManager.GetString("AdminPageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kullanıcılar.
        /// </summary>
        public static string AdminPageUsersSubtitle {
            get {
                return ResourceManager.GetString("AdminPageUsersSubtitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Şehir.
        /// </summary>
        public static string AdminUsersFormCity {
            get {
                return ResourceManager.GetString("AdminUsersFormCity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tarih.
        /// </summary>
        public static string AdminUsersFormDate {
            get {
                return ResourceManager.GetString("AdminUsersFormDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kullanıcıyı sil.
        /// </summary>
        public static string AdminUsersFormDeleteTooltip {
            get {
                return ResourceManager.GetString("AdminUsersFormDeleteTooltip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Değiştir.
        /// </summary>
        public static string AdminUsersFormModify {
            get {
                return ResourceManager.GetString("AdminUsersFormModify", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kullanıcının rolünü değiştir.
        /// </summary>
        public static string AdminUsersFormModifyRoleTooltip {
            get {
                return ResourceManager.GetString("AdminUsersFormModifyRoleTooltip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rol.
        /// </summary>
        public static string AdminUsersFormRole {
            get {
                return ResourceManager.GetString("AdminUsersFormRole", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kullanıcı adı.
        /// </summary>
        public static string AdminUsersFormUserName {
            get {
                return ResourceManager.GetString("AdminUsersFormUserName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Giyim öneri.
        /// </summary>
        public static string ClothingTip {
            get {
                return ResourceManager.GetString("ClothingTip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yorumlar.
        /// </summary>
        public static string Comments {
            get {
                return ResourceManager.GetString("Comments", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yorum.
        /// </summary>
        public static string CommentsComment {
            get {
                return ResourceManager.GetString("CommentsComment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Kaydet.
        /// </summary>
        public static string CommentsLeaveComment {
            get {
                return ResourceManager.GetString("CommentsLeaveComment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yazar hakkında.
        /// </summary>
        public static string ContactAboutAuthor {
            get {
                return ResourceManager.GetString("ContactAboutAuthor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Adres.
        /// </summary>
        public static string ContactAddress {
            get {
                return ResourceManager.GetString("ContactAddress", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mesajınızı yazınız.
        /// </summary>
        public static string ContactMessage {
            get {
                return ResourceManager.GetString("ContactMessage", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to İletişim sayfası.
        /// </summary>
        public static string ContactPageTitle {
            get {
                return ResourceManager.GetString("ContactPageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yazar.
        /// </summary>
        public static string FooterAuthor {
            get {
                return ResourceManager.GetString("FooterAuthor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Veri veren.
        /// </summary>
        public static string FooterDataProvider {
            get {
                return ResourceManager.GetString("FooterDataProvider", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeni şifreyi tekrarlayınız.
        /// </summary>
        public static string FormConfirmNewPassword {
            get {
                return ResourceManager.GetString("FormConfirmNewPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tekrarlanan şifre yeni şifreye uymuyor..
        /// </summary>
        public static string FormConfirmNewPasswordError {
            get {
                return ResourceManager.GetString("FormConfirmNewPasswordError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Şifreyi tekrarlayınız.
        /// </summary>
        public static string FormConfirmPassword {
            get {
                return ResourceManager.GetString("FormConfirmPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Şifreler birbirlerine uymuyor..
        /// </summary>
        public static string FormConfirmPasswordError {
            get {
                return ResourceManager.GetString("FormConfirmPasswordError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Şimdiki şifre.
        /// </summary>
        public static string FormCurrentPassword {
            get {
                return ResourceManager.GetString("FormCurrentPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Eposta.
        /// </summary>
        public static string FormEmail {
            get {
                return ResourceManager.GetString("FormEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ad.
        /// </summary>
        public static string FormFirstName {
            get {
                return ResourceManager.GetString("FormFirstName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Soyad.
        /// </summary>
        public static string FormLastName {
            get {
                return ResourceManager.GetString("FormLastName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeni şifre.
        /// </summary>
        public static string FormNewPassword {
            get {
                return ResourceManager.GetString("FormNewPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Şifre.
        /// </summary>
        public static string FormPassword {
            get {
                return ResourceManager.GetString("FormPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Şifre en azından 6 harfli olmalı.
        /// </summary>
        public static string FormPasswordError {
            get {
                return ResourceManager.GetString("FormPasswordError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Yeni üye kaydolunuz.
        /// </summary>
        public static string FormRegisterAsANewUser {
            get {
                return ResourceManager.GetString("FormRegisterAsANewUser", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Beni hatırla.
        /// </summary>
        public static string FormRemember {
            get {
                return ResourceManager.GetString("FormRemember", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Anasayfa.
        /// </summary>
        public static string HomePageTitle {
            get {
                return ResourceManager.GetString("HomePageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Hakkımızda.
        /// </summary>
        public static string MenuAbout {
            get {
                return ResourceManager.GetString("MenuAbout", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to İletişim.
        /// </summary>
        public static string MenuContact {
            get {
                return ResourceManager.GetString("MenuContact", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ana sayfa.
        /// </summary>
        public static string MenuHome {
            get {
                return ResourceManager.GetString("MenuHome", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Diller.
        /// </summary>
        public static string MenuLang {
            get {
                return ResourceManager.GetString("MenuLang", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to İngilizce.
        /// </summary>
        public static string MenuLangEnglish {
            get {
                return ResourceManager.GetString("MenuLangEnglish", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Fransızca.
        /// </summary>
        public static string MenuLangFrench {
            get {
                return ResourceManager.GetString("MenuLangFrench", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Türkçe.
        /// </summary>
        public static string MenuLangTurkish {
            get {
                return ResourceManager.GetString("MenuLangTurkish", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Giriş yap.
        /// </summary>
        public static string MenuSignIn {
            get {
                return ResourceManager.GetString("MenuSignIn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Çıkış.
        /// </summary>
        public static string MenuSignOut {
            get {
                return ResourceManager.GetString("MenuSignOut", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Üye ol.
        /// </summary>
        public static string MenuSignUp {
            get {
                return ResourceManager.GetString("MenuSignUp", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Giriş sayfa.
        /// </summary>
        public static string SignInPageTitle {
            get {
                return ResourceManager.GetString("SignInPageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Üye ol sayfa.
        /// </summary>
        public static string SignUpPageTitle {
            get {
                return ResourceManager.GetString("SignUpPageTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Graf.
        /// </summary>
        public static string WeatherInfosChart {
            get {
                return ResourceManager.GetString("WeatherInfosChart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Şehir.
        /// </summary>
        public static string WeatherInfosCity {
            get {
                return ResourceManager.GetString("WeatherInfosCity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Bulut.
        /// </summary>
        public static string WeatherInfosClouds {
            get {
                return ResourceManager.GetString("WeatherInfosClouds", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Şimdilik.
        /// </summary>
        public static string WeatherInfosCurrent {
            get {
                return ResourceManager.GetString("WeatherInfosCurrent", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Günlük.
        /// </summary>
        public static string WeatherInfosDaily {
            get {
                return ResourceManager.GetString("WeatherInfosDaily", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Açıklama.
        /// </summary>
        public static string WeatherInfosDescription {
            get {
                return ResourceManager.GetString("WeatherInfosDescription", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Saatlik.
        /// </summary>
        public static string WeatherInfosHourly {
            get {
                return ResourceManager.GetString("WeatherInfosHourly", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nem.
        /// </summary>
        public static string WeatherInfosHumidity {
            get {
                return ResourceManager.GetString("WeatherInfosHumidity", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ana.
        /// </summary>
        public static string WeatherInfosMain {
            get {
                return ResourceManager.GetString("WeatherInfosMain", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ara.
        /// </summary>
        public static string WeatherInfosSearch {
            get {
                return ResourceManager.GetString("WeatherInfosSearch", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rüzgar.
        /// </summary>
        public static string WeatherInfosWind {
            get {
                return ResourceManager.GetString("WeatherInfosWind", resourceCulture);
            }
        }
    }
}
